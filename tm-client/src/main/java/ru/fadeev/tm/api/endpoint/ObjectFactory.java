
package ru.fadeev.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.fadeev.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "Exception");
    private final static QName _GetHost_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "getHost");
    private final static QName _GetHostResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "getHostResponse");
    private final static QName _GetPort_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "getPort");
    private final static QName _GetPortResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "getPortResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.fadeev.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link GetHost }
     * 
     */
    public GetHost createGetHost() {
        return new GetHost();
    }

    /**
     * Create an instance of {@link GetHostResponse }
     * 
     */
    public GetHostResponse createGetHostResponse() {
        return new GetHostResponse();
    }

    /**
     * Create an instance of {@link GetPort }
     * 
     */
    public GetPort createGetPort() {
        return new GetPort();
    }

    /**
     * Create an instance of {@link GetPortResponse }
     * 
     */
    public GetPortResponse createGetPortResponse() {
        return new GetPortResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "getHost")
    public JAXBElement<GetHost> createGetHost(GetHost value) {
        return new JAXBElement<GetHost>(_GetHost_QNAME, GetHost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHostResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "getHostResponse")
    public JAXBElement<GetHostResponse> createGetHostResponse(GetHostResponse value) {
        return new JAXBElement<GetHostResponse>(_GetHostResponse_QNAME, GetHostResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPort }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "getPort")
    public JAXBElement<GetPort> createGetPort(GetPort value) {
        return new JAXBElement<GetPort>(_GetPort_QNAME, GetPort.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPortResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "getPortResponse")
    public JAXBElement<GetPortResponse> createGetPortResponse(GetPortResponse value) {
        return new JAXBElement<GetPortResponse>(_GetPortResponse_QNAME, GetPortResponse.class, null, value);
    }

}
