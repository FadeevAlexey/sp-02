package ru.fadeev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.fadeev.tm.entity.User;

public interface IUserRepository extends JpaRepository<User,String>, JpaSpecificationExecutor<User> {

}