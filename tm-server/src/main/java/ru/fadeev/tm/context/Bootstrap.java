package ru.fadeev.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.endpoint.AbstractEndpoint;

import javax.annotation.PostConstruct;
import javax.xml.ws.Endpoint;
import java.util.List;

@Getter
@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Value("${server.host}")
    private String serverHost;

    @NotNull
    @Value("${server.port}")
    private String serverPort;

    @NotNull
    @Value("${jms.bindaddress}")
    private String bindAddress;

    @NotNull
    @Autowired
    private List<AbstractEndpoint> endpointList;

    @PostConstruct
    public void init() {
        startBrokerService();
        start();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = System.getProperty("server.host");
        serverHost = host == null ? serverHost : host;
        @NotNull final String port = System.getProperty("server.port");
        serverPort = port == null ? serverPort : port;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String link = String.format(
                "http://%s:%s/%s?wsdl", serverHost, serverPort, name);
        System.out.println(link);
        Endpoint.publish(link, endpoint);
    }

    public void start() {
        endpointList.forEach(this::registry);
    }

    private void startBrokerService() {
        @NotNull final BrokerService broker = new BrokerService();
        try {
            broker.addConnector(bindAddress);
            broker.start();
        } catch (Exception ignored) {
        }
    }

}