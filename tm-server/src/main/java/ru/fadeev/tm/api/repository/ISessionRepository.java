package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.fadeev.tm.entity.Session;

public interface ISessionRepository extends JpaRepository<Session,String>, JpaSpecificationExecutor<Session> {

    void removeSessionBySignature(@NotNull String signature);

}