package ru.fadeev.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.endpoint.*;

@Configuration
public class AppConfig {

    @Bean
    @NotNull
    public ISessionEndpoint sessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Bean
    @NotNull
    public ISystemEndpoint systemEndpoint() {
        return new SystemEndpointService().getSystemEndpointPort();
    }

}
