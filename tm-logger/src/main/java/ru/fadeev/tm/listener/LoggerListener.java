package ru.fadeev.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.constant.Data;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

public class LoggerListener implements MessageListener {

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String destination = message.getJMSDestination().toString();
        saveToFile(textMessage.getText(), getPath(destination));
    }

    private void saveToFile(@NotNull final String message, @Nullable final Path file) throws IOException {
        if (file == null) return;
        try (@NotNull final FileWriter fileWriter = new FileWriter(file.toFile(), true)) {
            fileWriter.write(message);
            fileWriter.flush();
        }
    }

    @Nullable
    private Path getPath(@Nullable String destination) {
        if (destination == null || destination.isEmpty()) return null;
        destination = destination.replaceAll("topic://", "");
        if (destination.equals("Project")) return Data.PROJECT_LOG;
        if (destination.equals("Task")) return Data.TASK_LOG;
        if (destination.equals("User")) return Data.USER_LOG;
        if (destination.equals("Session")) return Data.SESSION_LOG;
        return null;
    }

}