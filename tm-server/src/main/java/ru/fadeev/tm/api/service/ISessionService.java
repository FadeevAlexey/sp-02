package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.enumerated.Role;

public interface ISessionService extends IService<Session> {

    void closeSession(@Nullable SessionDTO session);

    boolean contains(@Nullable String sessionId);

    @NotNull
    SessionDTO checkSession(@Nullable final SessionDTO currentSession);

    @NotNull
    SessionDTO checkSession(SessionDTO currentSession, @NotNull final Role role);

    @Nullable
    Session openSession(@Nullable final String login, @Nullable String password);

}