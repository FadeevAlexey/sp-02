package ru.fadeev.tm.command.application;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;

@Component
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand abstractCommand : appStateService.getCommands())
            terminal.println(String.format("%s: %s",abstractCommand.getName(),abstractCommand.getDescription()));
    }

}