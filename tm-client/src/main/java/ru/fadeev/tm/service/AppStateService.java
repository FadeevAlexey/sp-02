package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.fadeev.tm.api.repository.IAppStateRepository;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;

import java.util.*;

@Service
public class AppStateService  implements IAppStateService {

    @Autowired
    private IAppStateRepository appStateRepository;

    @Override
    public void putCommand(@Nullable final String description, @Nullable final AbstractCommand abstractCommand) {
        if (description == null || description.isEmpty()) return;
        if (abstractCommand == null) return;
        appStateRepository.putCommand(description, abstractCommand);
    }

    @Override
    @Nullable
    public AbstractCommand getCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return null;
        return appStateRepository.getCommand(command);
    }

    @Override
    @NotNull
    public List<AbstractCommand> getCommands() {
        return appStateRepository.getCommands();
    }

    @Override
    public @Nullable String getToken() {
        return appStateRepository.getToken();
    }

    @Override
    public void setToken(@Nullable String token) {
        appStateRepository.setToken(token);
    }

}