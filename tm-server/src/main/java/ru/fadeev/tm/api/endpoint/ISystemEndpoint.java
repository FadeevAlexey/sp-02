package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISystemEndpoint {

    @NotNull
    @WebMethod
    String getHost(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    String getPort(@WebParam(name = "token") String token) throws Exception;

}
