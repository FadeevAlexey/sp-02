package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.endpoint.TaskDTO;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalTaskNameException;

@Component
public final class TaskAddProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Override
    public String getName() {
        return "task-addProject";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Adding task to the project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[ADD TASK TO PROJECT]");
        terminal.println("ENTER TASK NAME");
        @Nullable final String taskId = taskEndpoint.findIdByNameTask(token, terminal.readString());
        terminal.println("ENTER PROJECT NAME");
        @Nullable final String projectId = projectEndpoint.findIdByNameProject(token, terminal.readString());
        if (taskId == null || projectId == null) throw new IllegalTaskNameException("Can't find project or task");
        @Nullable final TaskDTO task = taskEndpoint.findOneTask(token, taskId);
        if (task == null) throw new IllegalTaskNameException("Can't find project or task");
        task.setProjectId(projectId);
        taskEndpoint.mergeTask(token, task);
        terminal.println("[OK]\n");
    }

}