package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.Exception_Exception;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.endpoint.UserDTO;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.InvalidSessionException;
import ru.fadeev.tm.util.PasswordHashUtil;

@Component
public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "user-updatePassword";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User Password Changes.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        @Nullable final UserDTO currentUser = userEndpoint.findOneUser(token);
        if (currentUser == null) throw new InvalidSessionException("You were logout. Please log in again");
        terminal.println("[UPDATE PASSWORD]");
        terminal.println("Enter new password");
        @Nullable final String newPassword = PasswordHashUtil.md5(terminal.readString());
        currentUser.setPasswordHash(newPassword);
        userEndpoint.mergeUser(token, currentUser);
        terminal.println("[OK]\n");
    }

}