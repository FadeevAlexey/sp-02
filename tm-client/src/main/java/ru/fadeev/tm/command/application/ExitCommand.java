package ru.fadeev.tm.command.application;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.command.AbstractCommand;

@Component
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit from the program.";
    }

    @Override
    public void execute() {
        terminal.println("Thank you for using Task Manager. See you soon.");
    }

}