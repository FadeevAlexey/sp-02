package ru.fadeev.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.CommandCorruptException;
import ru.fadeev.tm.exception.IllegalCommandNameException;

import javax.annotation.PostConstruct;
import java.lang.Exception;
import java.util.List;

@Getter
@Setter
@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private List<AbstractCommand> commandList;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Autowired
    private ITerminalService terminalService;

    @PostConstruct
    public void init() {
        initCommand();
        start();
    }

    private void initCommand() {
        commandList.forEach(this::registry);
    }

    private void start() {
        terminalService.println("*** WELCOME TO TASK MANAGER ***");
        @Nullable String command = "";
        while (!"exit".equals(command)) {
            try {
                command = getTerminalService().readString();
                execute(command);
            } catch (final Exception e) {
                terminalService.println(e.getMessage());
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        @Nullable final String cliCommand = command.getName();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand.isEmpty()) throw new CommandCorruptException();
        if (cliDescription.isEmpty()) throw new CommandCorruptException();
        appStateService.putCommand(cliCommand, command);
    }

    private void execute(@Nullable String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = appStateService.getCommand(command);
        if (abstractCommand == null) throw new IllegalCommandNameException("Wrong command name");
        abstractCommand.execute();
    }

}