package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.ProjectDTO;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalSearchRequestException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Component
public final class ProjectSearchCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Override
    public String getName() {
        return "project-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for projects by NAME or DESCRIPTION.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT SEARCH]");
        terminal.println("SEARCH REQUEST");
        @Nullable final String searchRequest = terminal.readString();
        if (searchRequest == null || searchRequest.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.println("Select search type: NAME, DESCRIPTION, ALL");
        @Nullable final String typeSearch = terminal.readString();
        if (typeSearch == null || typeSearch.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.printProjectList(getProjectList(token, searchRequest, typeSearch)
        );
    }

    @NotNull
    private Collection<ProjectDTO> getProjectList(
            @NotNull final String  token,
            @NotNull final String searchRequest,
            @NotNull final String typeSearch
    ) throws Exception {
        switch (typeSearch.toLowerCase()) {
            case "name":
                return projectEndpoint.searchByNameProject(token, searchRequest);
            case "description":
                return projectEndpoint.searchByDescriptionProject(token, searchRequest);
            case "all": {
                @NotNull final Collection<ProjectDTO> searchByName =
                        projectEndpoint.searchByNameProject(token, searchRequest);
                @NotNull final Collection<ProjectDTO> searchByDescription =
                        projectEndpoint.searchByDescriptionProject(token, searchRequest);
                @NotNull final Set<ProjectDTO> allResult = new HashSet<>();
                allResult.addAll(searchByName);
                allResult.addAll(searchByDescription);
                return allResult;
            }
            default:
                throw new IllegalSearchRequestException("invalid search type");
        }
    }

}