package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.ISessionRepository;
import ru.fadeev.tm.api.service.ISessionService;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.InvalidSessionException;
import ru.fadeev.tm.util.PasswordHashUtil;
import ru.fadeev.tm.util.SignatureUtil;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository sessionRepository;

    @Autowired
    private IUserService userService;

    @NotNull
    @Value("${session.salt}")
    private String sessionSalt;

    @NotNull
    @Value("${session.cycle}")
    private Integer sessionCycle;

    @NotNull
    @Value("${session.lifetime}")
    private Integer getSessionLifetime;

    @NotNull
    @Override
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return sessionRepository.findById(id).orElse(null);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        sessionRepository.deleteById(id);
    }

    @Override
    public void persist(@Nullable final Session session) {
        if (session == null) return;
        sessionRepository.save(session);
    }

    @Override
    public void merge(@Nullable final Session session) {
        if (session == null) return;
        sessionRepository.save(session);
    }

    @Override
    public void removeAll() {
        sessionRepository.deleteAllInBatch();
    }

    @Override
    public void closeSession(@Nullable final SessionDTO session) {
        if (session == null) return;
        if (session.getSignature() == null) return;
        sessionRepository.removeSessionBySignature(session.getSignature());
    }

    @Override
    public boolean contains(@Nullable final String sessionId) {
        if (sessionId == null) return false;
       return sessionRepository.existsById(sessionId);
    }

    @NotNull
    public Session openSession(@Nullable final String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new InvalidSessionException("Bad login or password");
        if (password == null || password.isEmpty()) throw new InvalidSessionException("Bad login or password");
        @Nullable final User user = userService.findUserByLogin(login);
        if (user == null) throw new InvalidSessionException("Bad login or password");
        if (!PasswordHashUtil.md5(password).equals(user.getPasswordHash()))
            throw new InvalidSessionException("Bad login or password");
        Session session = new Session();
        session.setUser(user);
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session, sessionSalt, sessionCycle));
        persist(session);
        return session;
    }

    @NotNull
    public SessionDTO checkSession(final SessionDTO currentSession) {
        final long currentTime = new Date().getTime();
        if (currentSession == null) throw new InvalidSessionException();
        if (!contains(currentSession.getId())) throw new InvalidSessionException("Invalid session");
        if (currentSession.getUserId() == null) throw new InvalidSessionException();
        if (currentSession.getSignature() == null) throw new InvalidSessionException();
        if (currentSession.getRole() == null) throw new InvalidSessionException();
        @NotNull final SessionDTO testSession = new SessionDTO();
        testSession.setSignature(currentSession.getSignature());
        testSession.setRole(currentSession.getRole());
        testSession.setId(currentSession.getId());
        testSession.setCreationTime(currentSession.getCreationTime());
        testSession.setUserId(currentSession.getUserId());
        @Nullable final String sessionSignature =
                SignatureUtil.sign(testSession, sessionSalt, sessionCycle);
        @Nullable final String currentSessionSignature =
                SignatureUtil.sign(currentSession, sessionSalt, sessionCycle);
        if (sessionSignature == null || currentSessionSignature == null)
            throw new InvalidSessionException("Invalid session");
        if (!sessionSignature.equals(currentSessionSignature)) throw new InvalidSessionException("Invalid session");
        if (currentSession.getCreationTime() - currentTime > getSessionLifetime)
            throw new InvalidSessionException("Invalid session");
        return currentSession;
    }

    @NotNull
    public SessionDTO checkSession(final SessionDTO session, @NotNull final Role role) {
        if (session == null) throw new InvalidSessionException("Invalid session");
        checkSession(session);
        if (session.getRole() != role) throw new AccessDeniedException("Access denied");
        return session;
    }

}