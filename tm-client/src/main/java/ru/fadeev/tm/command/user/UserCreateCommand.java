package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.util.PasswordHashUtil;

import java.lang.Exception;

@Component
public final class UserCreateCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "user-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "creates a new user account";
    }

    @Override
    public void execute() throws Exception {
        terminal.println("[CREATE ACCOUNT]");
        terminal.println("ENTER NAME");
        @Nullable final String login = terminal.readString();
        @NotNull boolean isLoginExist = userEndpoint.isLoginExistUser(login);
        if (login == null || login.isEmpty()) throw new Exception("Incorrect name");
        if (isLoginExist) throw new Exception("User with same name already exist");
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        terminal.println("Enter password:");
        user.setPasswordHash(PasswordHashUtil.md5(terminal.readString()));
        userEndpoint.persistUser(user);
        terminal.print("[OK]\n");
    }

}