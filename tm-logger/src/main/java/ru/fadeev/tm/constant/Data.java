package ru.fadeev.tm.constant;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Data {

    public static final Path DATA_FOLDER = Paths.get(System.getProperty("user.dir") + File.separator + "datalog");

    public static final Path TASK_LOG = Paths.get(DATA_FOLDER + File.separator +"task.log");

    public static final Path USER_LOG = Paths.get(DATA_FOLDER + File.separator +"user.log");

    public static final Path PROJECT_LOG = Paths.get(DATA_FOLDER + File.separator +"project.log");

    public static final Path SESSION_LOG = Paths.get(DATA_FOLDER + File.separator +"session.log");

}