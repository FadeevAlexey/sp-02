package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.fadeev.tm.entity.Project;

public interface IProjectRepository extends JpaRepository<Project, String>, JpaSpecificationExecutor<Project> {

    void removeByUser_IdAndId(@NotNull String userId, @NotNull String id);

    void removeAllByUser_Id(@NotNull String userId);

}