package ru.fadeev.tm.command.application;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.Exception_Exception;
import ru.fadeev.tm.api.endpoint.ISystemEndpoint;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.service.AppStateService;

@Component
public final class ServerInfoCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private AppStateService appStateService;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "server-info";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Port and host information.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[SERVER-INFO]");
        terminal.println("HOST: " + systemEndpoint.getHost(token));
        terminal.println("PORT: " + systemEndpoint.getPort(token));
        terminal.println("[OK]\n");
    }

}