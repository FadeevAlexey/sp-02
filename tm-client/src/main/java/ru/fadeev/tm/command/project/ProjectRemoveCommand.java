package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalProjectNameException;

@Component
public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT REMOVE]\nENTER NAME");
        @Nullable final String projectId
                = projectEndpoint.findIdByNameProject(token, terminal.readString());
        if (projectId == null) throw new IllegalProjectNameException("Can't find project");
        projectEndpoint.removeProject(token, projectId);
        taskEndpoint.removeAllByProjectIdTask(token, projectId);
        terminal.println("[OK]\n");
    }

}