package ru.fadeev.tm.endpoint;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.util.EncryptUtil;

public class AbstractEndpoint {

    @NotNull
    @Value("${token.secretkey}")
    private String secretKey;

    protected String cryptSession(@Nullable final SessionDTO session) throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return EncryptUtil.encrypt(json, secretKey);
    }

    protected SessionDTO decryptSession(@Nullable final String cryptSession) throws Exception {
        @NotNull final String json = EncryptUtil.decrypt(cryptSession, secretKey);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final SessionDTO session = mapper.readValue(json, SessionDTO.class);
        return session;
    }

}