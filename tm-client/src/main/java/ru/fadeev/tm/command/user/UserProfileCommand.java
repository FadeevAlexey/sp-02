package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.endpoint.UserDTO;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;

@Component
public final class UserProfileCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show current profile.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        @Nullable final UserDTO user = userEndpoint.findOneUser(token);
        if (user == null) throw new AccessDeniedException("Access denied");
        terminal.println("[USER PROFILE]");
        terminal.println(String.format(
                "name: %s, role: %s, id: %s",
                user.getLogin(), user.getRole(), user.getId()) + "\n");
        terminal.println("IF YOU'D LIKE UPDATE PROFILE USE COMMAND: user-edit\n");
    }

}