package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.endpoint.TaskDTO;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalSearchRequestException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Component
public final class TaskSearchCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Override
    public final String getName() {
        return "task-search";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Search for tasks by NAME or DESCRIPTION.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[TASK SEARCH]");
        terminal.println("SEARCH REQUEST");
        @Nullable final String searchRequest = terminal.readString();
        if (searchRequest == null || searchRequest.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.println("Select search type: NAME, DESCRIPTION, ALL");
        @Nullable final String typeSearch = terminal.readString();
        if (typeSearch == null || typeSearch.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.printTaskList(getTaskList(token, searchRequest, typeSearch));
    }

    @NotNull
    private Collection<TaskDTO> getTaskList(
            @NotNull final String token,
            @NotNull final String searchRequest,
            @NotNull final String typeSearch
    ) throws Exception {
        switch (typeSearch.toLowerCase()) {
            case "name": return taskEndpoint.searchByNameTask(token, searchRequest);
            case "description": return taskEndpoint.searchByDescriptionTask(token, searchRequest);
            case "all": {
                @NotNull final Collection<TaskDTO> searchByName =
                        taskEndpoint.searchByNameTask(token, searchRequest);
                @NotNull final Collection<TaskDTO> searchByDescription =
                        taskEndpoint.searchByDescriptionTask(token, searchRequest);
                @NotNull final Set<TaskDTO> allResult = new HashSet<>();
                allResult.addAll(searchByName);
                allResult.addAll(searchByDescription);
                return allResult;
            }
            default: throw new IllegalSearchRequestException("invalid search type");
        }
    }

}