package ru.fadeev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ru.fadeev.tm.api.endpoint.ISystemEndpoint;
import ru.fadeev.tm.api.service.ISessionService;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.ISystemEndpoint")
public class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Autowired
    protected ISessionService sessionService;

    @NotNull
    @Value("${server.host}")
    private String serverHost;

    @NotNull
    @Value("${server.port}")
    private String serverPort;

    @NotNull
    @WebMethod
    public String getHost(@WebParam(name = "token") @Nullable final String token) throws Exception {
        sessionService.checkSession(decryptSession(token), Role.ADMINISTRATOR);
        @NotNull final String host = System.getProperty("server.host");
        serverHost = host == null ? serverHost : host;
        return serverHost;
    }

    @NotNull
    @WebMethod
    public String getPort(@WebParam(name = "token") @Nullable final String token) throws Exception {
        sessionService.checkSession(decryptSession(token), Role.ADMINISTRATOR);
        @NotNull final String port = System.getProperty("server.port");
        serverPort = port == null ? serverPort : port;
        return serverPort;
    }

}