package ru.fadeev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ISessionService;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    protected IUserService userService;

    @NotNull
    @Autowired
    protected ISessionService sessionService;

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjectAdmin(@WebParam(name = "token") final String token) throws Exception {
        sessionService.checkSession(decryptSession(token), Role.ADMINISTRATOR);
        return projectService.findAll()
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findOneProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        @Nullable final Project project = projectService.findOne(currentSession.getUserId(), projectId);
        return convertToDTO(project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        projectService.remove(currentSession.getUserId(), id);
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "project") @NotNull final ProjectDTO projectDTO
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        projectDTO.setUserId(currentSession.getUserId());
        projectService.persist(convertToProject(projectDTO));
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "project") @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        if (projectDTO == null) return;
        @NotNull final SessionDTO session = sessionService.checkSession(decryptSession(token));
        if (session.getUserId().equals(projectDTO.getUserId()))
            projectService.merge(convertToProject(projectDTO));
    }

    @Override
    @WebMethod
    public void removeAllProjectAdmin(
            @WebParam(name = "token") final String token) throws Exception {
        sessionService.checkSession(decryptSession(token), Role.ADMINISTRATOR);
        projectService.removeAll();
    }

    @Override
    @Nullable
    @WebMethod
    public String findIdByNameProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return projectService.findIdByName(currentSession.getUserId(), name);
    }

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return projectService.findAll(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }


    @Override
    @WebMethod
    public void removeAllProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        projectService.removeAll(currentSession.getUserId());
    }

    @NotNull
    public Collection<ProjectDTO> sortByStartDateProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return projectService.sortByStartDate(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    public Collection<ProjectDTO> sortByFinishDateProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return projectService.sortByFinishDate(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    public Collection<ProjectDTO> sortByCreationTimeProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return projectService.sortByCreationDate(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    public Collection<ProjectDTO> sortByStatusProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return projectService.sortByStatus(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDTO> searchByNameProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return projectService.searchByName(currentSession.getUserId(), string)
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDTO> searchByDescriptionProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return projectService.searchByDescription(currentSession.getUserId(), string)
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    private Project convertToProject(@Nullable final ProjectDTO projectDTO) throws Exception {
        if (projectDTO == null) return null;
        @Nullable final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setStartDate(projectDTO.getStartDate());
        project.setFinishDate(projectDTO.getFinishDate());
        User user = userService.findOne(projectDTO.getUserId());
        if (user == null) return null;
        project.setUser(user);
        project.setStatus(projectDTO.getStatus());
        project.setCreationTime(projectDTO.getCreationTime());
        return project;
    }

    @Nullable
    private ProjectDTO convertToDTO(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setStartDate(project.getStartDate());
        projectDTO.setFinishDate(project.getFinishDate());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setCreationTime(project.getCreationTime());
        return projectDTO;
    }

}