package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.specification.Specifications;

import java.util.List;

@Service
@Transactional
public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Override
    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        userRepository.deleteById(id);
    }

    @Override
    public void removeAll() {
        userRepository.deleteAllInBatch();
    }

    @Override
    public void persist(@Nullable final User user) {
        if (user == null) return;
        userRepository.save(user);
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        userRepository.save(user);
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.findAll(Specifications.isLoginExist(login)).size() > 0;
    }

    @Override
    @Nullable
    public User findUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findAll(Specifications.findUserByLogin(login))
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void setAdminRole(@Nullable final User user) {
        if (user == null) return;
        user.setRole(Role.ADMINISTRATOR);
        merge(user);
    }

}