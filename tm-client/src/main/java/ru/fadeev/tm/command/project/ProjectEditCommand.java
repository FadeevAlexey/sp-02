package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.ProjectDTO;
import ru.fadeev.tm.api.endpoint.Status;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalProjectNameException;

import javax.xml.datatype.XMLGregorianCalendar;

@Component
public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[EDIT PROJECT]");
        terminal.println("ENTER CURRENT NAME:");
        @Nullable final String projectId = projectEndpoint.findIdByNameProject(token, terminal.readString());
        if (projectId == null) throw new IllegalProjectNameException("Can't find project");
        @Nullable final ProjectDTO project = projectEndpoint.findOneProject(token, projectId);
        if (project == null) throw new IllegalProjectNameException("Can't find project");
        fillFields(project);
        projectEndpoint.mergeProject(token, project);
        terminal.println("[OK]\n");
    }

    private void fillFields(@NotNull final ProjectDTO project) throws Exception {
        terminal.println("YOU CAN CHANGE PROPERTIES OR PRESS ENTER");
        terminal.println("CHOOSE AN STATUS DESIRED: Planned, In progress, Done");
        @Nullable String status = terminal.readString();
        if (status != null && !status.isEmpty()) project.setStatus(Status.fromValue(status.toUpperCase()));
        terminal.println("CHANGE DESCRIPTION:");
        @Nullable final String description = terminal.readString();
        terminal.println("CHANGE START DATE:");
        final @Nullable XMLGregorianCalendar startDate = terminal.readDate();
        terminal.println("CHANGE FINISH DATE:");
        final @Nullable XMLGregorianCalendar finishDate = terminal.readDate();
        if (description != null && !description.isEmpty()) project.setDescription(description);
        if (startDate != null) project.setStartDate(startDate);
        if (finishDate != null) project.setFinishDate(finishDate);
    }

}