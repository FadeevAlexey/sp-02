package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.ISessionEndpoint;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;

@Component
public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "logout from task-manager";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        sessionEndpoint.closeSession(token);
        appStateService.setToken(null);
        terminal.println("[OK]\n");
    }

}