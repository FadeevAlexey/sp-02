package ru.fadeev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper=true)
public class SessionDTO extends AbstractEntityDTO {

    @Nullable
    String userId;

    @Nullable
    String signature;

    long creationTime = new Date().getTime();

    @NotNull
    Role role;

}