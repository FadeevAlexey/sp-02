package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.endpoint.UserDTO;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.util.PasswordHashUtil;

@Component
public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAppStateService appStateService;


    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit user profile";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        @Nullable final UserDTO currentUser = userEndpoint.findOneUser(token);
        if (currentUser == null) throw new Exception("Can't find user");
        terminal.println("[EDIT PROFILE]");
        terminal.println("ENTER NEW NAME OR PRESS ENTER");
        @Nullable final String name = terminal.readString();
        if (userEndpoint.isLoginExistUser(name))
            throw new Exception("User with same name already exist");
        terminal.println("ENTER NEW PASSWORD OR PRESS ENTER");
        @Nullable final String newPassword = PasswordHashUtil.md5(terminal.readString());
        if (newPassword != null && !newPassword.isEmpty()) currentUser.setPasswordHash(newPassword);
        if (name != null && !name.isEmpty()) currentUser.setLogin(name);
        userEndpoint.mergeUser(token, currentUser);
        terminal.println("[OK]\n");
    }

}