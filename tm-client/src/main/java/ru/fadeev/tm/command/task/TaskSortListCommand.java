package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.endpoint.TaskDTO;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.constant.Sort;
import ru.fadeev.tm.exception.AccessDeniedException;

import java.util.Collection;
import java.util.Collections;

@Component
public final class TaskSortListCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-sortList";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Shows sorted tasks by START DATE, FINISH DATE, STATUS.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[TASK SORT LIST]");
        terminal.println("Select sorting type:\n" +
                "START DATE, FINISH DATE, STATUS or press ENTER for default sort by adding");
        @Nullable final String sortRequest = terminal.readString();
        terminal.printTaskList(sortAll(token, sortRequest));
    }

    public Collection<TaskDTO> sortAll(@Nullable final String token, @Nullable String sortRequest) throws Exception {
        if (token == null || token.isEmpty()) return Collections.emptyList();
        if (sortRequest == null || sortRequest.isEmpty())
            return taskEndpoint.sortByCreationTimeTask(token);
        if (sortRequest.toUpperCase().contains(Sort.SUFFIX)) {
            sortRequest = sortRequest.substring(0, sortRequest.length() - Sort.SUFFIX.length());
        }
        switch (sortRequest.toLowerCase()) {
            case "start date":
                return taskEndpoint.sortByStartDateTask(token);
            case "finish date":
                return taskEndpoint.sortByFinishDateTask(token);
            case "status":
                return taskEndpoint.sortByStatusTask(token);
            default:
                throw new Exception("cannot sort");
        }
    }

}