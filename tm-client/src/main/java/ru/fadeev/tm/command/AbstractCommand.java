package ru.fadeev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.fadeev.tm.service.TerminalService;

public abstract class AbstractCommand {

    @NotNull
    @Autowired
    protected TerminalService terminal;

    public abstract void execute() throws Exception;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

}