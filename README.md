###### [https://gitlab.com/FadeevAlexey/sp-02](https://gitlab.com/FadeevAlexey/sp-02)
# Task Manager 1.1.2

A simple console task manager, can help you organize your tasks.

### Built with
  - Java 8
  - Maven 4.0

### Developer
Alexey Fadeev
[alexey.v.fadeev@gmail.com](mailto:alexey.v.fadeev@gmail.com?subject=TaskManager)

### Building from source

```sh
$ git clone http://gitlab.volnenko.school/FadeevAlexey/sp-02.git
$ cd sp-02
$ mvn clean
$ mvn install
```

### Server running

```sh
$ java -jar tm-server/target/release/bin/tm-server.jar
```

### Client running

```sh
$ java -jar tm-client/target/release/bin/tm-client.jar
```

### Logger running

```sh
$ java -jar tm-logger/target/release/bin/tm-logger.jar
```

