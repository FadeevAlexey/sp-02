package ru.fadeev.tm.command.application;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.command.AbstractCommand;

@Component
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Product information.";
    }

    @Override
    public void execute() {
        terminal.println("*** ABOUT TASK MANAGER ***");
        terminal.println("Developer:" + Manifests.read("developer") + " " + Manifests.read("email"));
        terminal.println("Product: " + Manifests.read("artifactId"));
        terminal.println("Version: " + Manifests.read("version"));
        terminal.println("Build number: " + Manifests.read("buildNumber"));
        terminal.println("(C) 2020\n");
    }

}